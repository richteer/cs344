#include "common.h"


void siftDown(int * array, int start, int end)
{
    int root = start;
	int child, sw;

    while (root * 2 + 1 <= end) {
        child = root * 2 + 1;
        sw = root;

        if (lessthan(array[sw],array[child]))
            sw = child;
        if ((child+1 <= end) && lessthan(array[sw], array[child+1]))
            sw = child + 1;
        if (sw != root) {
            swap(array+root, array+sw);
            root = sw;
		}
        else
            return;
	}
}


void heapify(int * array, int size)
{
    int start = (size - 2 ) / 2;
    
    while (start >= 0) {
        siftDown(array, start, size-1);
        start--;
	}
}


void sort(int * array, int size)
{
	int i;
	int end; 

    heapify(array, size);

    end = size - 1;
    while (end > 0) {
        swap(array, array+end);
        end--;
        siftDown(array, 0, end);
	}
}

#include "main.h"
