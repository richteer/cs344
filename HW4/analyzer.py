import sys
from random import randint
from subprocess import check_output as px
import matplotlib.pyplot as plt
from math import log


funcs = {
	"insertion":([],'x**2'),
	"selection":([],'x**2'),
	"quicksort":([],'x*log2(x)'),
	"rquicksort":([],'x*log2(x)'),
	"mergesort":([],'x*log2(x)'),
	"heapsort":([],'x*log2(x)'),
}

iters = 30


def mean(ls):
	res = 0.0

	for l in ls:
		res += l

	return res / len(ls)

def log2(x):
	return log(x,2)

if len(sys.argv) == 2:
	mx = int(sys.argv[1])+1
else:
	mx = 1025

for s in funcs:
	for i in range(2,mx):
		print("\rRunning {0} for N={1}".format(s,i),end='')

		vals = []	
		for j in range(iters):
			arr = []
			for c in range(i):
				arr.append(str(randint(1,100)))

			out = px(["./{0}".format(s)]+arr,universal_newlines=True).split("\n")
			vals.append(int(out[-2]))
		funcs[s][0].append(mean(vals))
	print("\rFinished {0}                   ".format(s))
print("")
px = [i for i in range(2,mx)]

fig = plt.figure()
axes = fig.add_axes([0.1,0.1,0.8,0.8])
'''
axes.plot(x,s_data,'b',label='Select')
axes.plot(x,q_data,'r',label='QuickSort')
'''

for f in funcs:
	axes.plot(px,funcs[f][0],label=f)

h, l = axes.get_legend_handles_labels()
axes.legend(h,l,loc=2)
axes.set_xlabel('Size of Array')
axes.set_ylabel('# of Comparisons')
axes.set_title('Sorting Algorithms')
axes.legend()
fig.savefig('kitchensink.png')

for f in funcs:
	fig = plt.figure()
	axes = fig.add_axes([0.1,0.1,0.8,0.8])
	axes.plot(px,funcs[f][0],'b',label=f)
	axes.plot(px,[eval(funcs[f][1]) for x in px], 'r',label=funcs[f][1])
	h, l = axes.get_legend_handles_labels()
	axes.legend(h,l,loc=2)
	axes.set_xlabel('Size of Array')
	axes.set_ylabel('# of Comparisons')
	axes.set_title(f)
	fig.savefig('figures/' + f +'.png')
