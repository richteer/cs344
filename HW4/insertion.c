#include "common.h"

void sort_left(int * array, int offset)
{
	if (offset == 0) return;
	if (lessthan(array[offset-1],array[offset])) return;

	swap(array + offset -1, array + offset);

	sort_left(array,offset-1);
}

void sort_helper(int * array, int offset, int size)
{
	if (offset >= size) return;

	sort_left(array, offset);
	sort_helper(array, offset+1, size);	

}

int sort(int * array, int size)
{
	sort_helper(array,1,size);

	return 0;
}

#include "main.h"
