.LC0:
	.string	"%d\n"
	.text
	.globl	main
	.type	main, @function

// Note: this is an in-place sort
sort:
	pushl	%ebp
	movl	%esp, %ebp

	// Move the number of elements into %eax, gets decremented, return 0 if good
	movl	8(%ebp), %eax
	
	// Reference to the array
	movl	12(%ebp), %ecx 
	
	cmpl	$0, %eax
	jg .CON1
	leave
	ret
.CON1:
	movl $1, %eax
	// Current lowest value's address
	leal	(%ecx), %ebx
	// Note: edx is current value
	decl	%eax	

	// Move the first into the temp
	movl	(%ebx), %edx
.L1:
	movl	(%ecx,%eax, 4), %edx
	cmpl	(%ebx), %edx

// --- REMOVE BELOW TO FIX THE SORTING
	pushl	%edi
	movl	comparisons, %edi
	incl	%edi
	movl	%edi, comparisons
	popl	%edi	
// --- END REMOVE

	jge .L2
	leal	(%ecx,%eax,4), %ebx
	
.L2:
	incl	%eax
	cmpl	8(%ebp), %eax
	jle .L1

	movl	(%ebx), %edx
	movl	(%ecx), %eax
	movl	%eax, (%ebx)
	movl	%edx, (%ecx)

	movl	12(%ebp), %edx
	leal	4(%edx), %edx
	pushl	%edx

	movl	8(%ebp), %edx
	decl	%edx
	pushl	%edx

	call	sort
	leave
	ret

main:
	pushl	%ebp
	movl	%esp, %ebp
	movl	8(%ebp), %ebx
	pushl	$1

.ML:
	movl	-4(%ebp), %edx
	movl	12(%ebp), %ecx
	pushl	(%ecx,%edx,4)
//	pushl	(%ecx,%edx,4)
	call atoi
	movl	%eax, (%esp)
	movl	8(%ebp), %ebx
	movl	-4(%ebp), %edx
	incl	%edx
	movl	%edx, -4(%ebp)
	cmpl	%edx,%ebx
	jne	.ML


	movl	$0, comparisons
//	pushl 	$.LC0
//	call printf
//	addl	$4, %esp	
	decl	%ebx
	pushl 	%esp
	pushl	%ebx
	call sort

	popl	%ebx
	popl	%ecx

	pushl	%ebx
	pushl	%ecx

	call print_array
	
	call print_end	

//	addl 	$8, %esp
//	pushl	$.LC0
//	call printf

	movl	$0, %eax

	leave
	ret

