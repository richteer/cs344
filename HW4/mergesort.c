#include "common.h"
#include <string.h>
#include <stdio.h>


void merge(int * array, int size)
{
	int *pleft, *pright;
	int sleft, sright;
	int *tmpleft,*tmpright,*fleft,*fright;
	int i;
	
	// Nothing to do here, return!
	if (size <= 2) {
		if (size <= 1) {
			return;
		}
		if (!lessthan(array[0],array[1])) {
			swap(array,array+1);
		}
		return;
	}

	// For sanity:
	pleft = array;
	pright = array + size/2;
	sleft = size/2;
	sright = (size+1)/2;
	
	merge(pleft, sleft);
	merge(pright, sright);

	fleft = tmpleft = malloc(sizeof(int)*sleft);
	fright = tmpright = malloc(sizeof(int)*sright);

	memcpy(tmpleft,pleft,sleft*sizeof(int));
	memcpy(tmpright,pright,sright*sizeof(int));

	for (i = 0; i < size; i++) { 
		if (!sleft) {
			array[i] = *(tmpright++);
		}
		else if (!sright) { 
			array[i] = *(tmpleft++);
		}
		else {
			array[i] = ( (lessthan(*tmpleft,*tmpright)) ? ((sleft--),*(tmpleft++)) : ((sright--),*(tmpright++)) );
		}
	}
	
	free(fleft);
	free(fright);
}

void sort(int * array, int size)
{
	
	merge(array, size);

}

#include "main.h"
