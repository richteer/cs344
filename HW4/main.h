#include <stdio.h>

int main(int argc, char ** argv)
{
	int * array;
	int i;

	if (argc == 1) {fprintf(stderr,"Usage: %s <nums to sort...>\n",argv[0]); return 1;}

	array = arg_parse(argc,argv);

	sort(array,argc-1);

	printf("[");
	for (i = 0; i < argc-2; i++) {
		printf("%d ",array[i]);
	} 
	printf("%d]\n",array[i]);

	print_end();

	free(array);
	return 0;
}
