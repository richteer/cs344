#include "common.h"
#include <string.h>
#include <stdlib.h>
// Deterministic pivot choice

typedef struct {
	int * buf;
	int size;
} side_t;

void append(side_t * s, int value)
{
	s->buf[(s->size)++] = value;
}

void init_side(side_t *s, int size)
{
	s->buf = malloc(sizeof(int) * size);
	s->size = 0;
}

void free_side(side_t *s)
{
	free(s->buf);
}

void quisort(int * array, int start, int end)
{
	int i;
	side_t left,right;
	int s_left, s_right;
	int pivot;

	if (start >= end) {/*printf("dumping\n");*/return;}
	
	// Move random index to front

	swap(array + start, array + ((rand() % (end-start+1)) + start));
	
	pivot = array[start];

	init_side(&left,end-start+1);
	init_side(&right,end-start+1);

	// +1 since pivot is first
	for (i = start + 1; i <= end; i++) {
		if (lessthan(array[i],pivot)) {
			append(&left, array[i]);
		} else {
			append(&right, array[i]);
		}
	}

	s_left = left.size;
	s_right = right.size;

//	printf("Size left: %d, Size right: %d\n",s_left,s_right);

	if (s_left)	
		memcpy(array + start,left.buf,sizeof(int)*s_left);
	array[s_left+start] = pivot;
	if (s_right)
		memcpy(array + start + 1 + s_left,right.buf,sizeof(int)*s_right);

//	print_array(array,start,end);

	free_side(&left);
	free_side(&right);
	
//	printf("Start left\n");
	quisort(array,start,start+s_left-1);
//	printf("Start right\n");
	quisort(array,s_left+1+start,end);
}


void sort(int * array, int size) {

	quisort(array, 0, size-1);	
}

#include "main.h"
