#include "common.h"
#include <string.h>
// Deterministic pivot choice

typedef struct {
	int * buf;
	int size;
} side_t;

void append(side_t * s, int value)
{
	s->buf[(s->size)++] = value;
}

void init_side(side_t *s, int size)
{
	s->buf = malloc(sizeof(int) * size);
	s->size = 0;
}

void free_side(side_t *s)
{
	free(s->buf);
}

void quisort(int * array, int start, int end)
{
	int i;
	side_t left,right;
	int s_left, s_right;
	int pivot = array[start];

	if (start >= end) return;

	init_side(&left,end-start+1);
	init_side(&right,end-start+1);

	// +1 since pivot is first
	for (i = start + 1; i <= end; i++) {
		if (lessthan(array[i],pivot)) {
			append(&left, array[i]);
		} else {
			append(&right, array[i]);
		}
	}

	s_left = left.size;
	s_right = right.size;

	if (s_left)	
		memcpy(array + start,left.buf,sizeof(int)*s_left);
	array[s_left+start] = pivot;
	if (s_right)
		memcpy(array + start + 1 + s_left,right.buf,sizeof(int)*s_right);


	free_side(&left);
	free_side(&right);
	
	quisort(array,start,start+s_left-1);
	quisort(array,s_left+1+start,end);
}


void sort(int * array, int size) {

	quisort(array, 0, size-1);	
}

#include "main.h"
