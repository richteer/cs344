#include <stdio.h>
#include "common.h"

int comparisons = 0;

int * arg_parse(int argc, char ** argv)
{
	int i;
	int * ret;

	ret = malloc((argc-1)*sizeof(int));

	for (i = 1; i < argc; i++) {
		ret[i-1] = atoi(argv[i]);
	}

	return ret;
}

int lessthan(int left, int right) {
	comparisons++;

	return left < right;
}

void swap(int * a, int * b)
{
	if (a == b) return;

	*a = *a ^ *b;
	*b = *a ^ *b;
	*a = *a ^ *b;
}

void print_array(int * array, int size)
{
	int i;
	printf("[");

	for (i = 0; i < size - 1 ; i++) {
		printf("%d ",array[i]);
	}
	printf("%d]\n",array[i++]);

}

void print_end(void)
{
	printf("%d\n",comparisons);

}
