#include <stdlib.h>
#include <stdio.h>
#include "quicksort.h"

extern int comparisons;

static int random_array(int *buf, size_t size)
{
	int i;
	if (NULL == buf) return -1;

	for (i = 0; i < size; i++)
		buf[i] = random() % 100 + 1;

	return 0;
}

static void print_array(int *buf, int size)
{
	int i;
	fprintf(stderr,"[");
	for (i = 0; i < size-1; i++)
		fprintf(stderr,"%d, ",buf[i]);

	fprintf(stderr,"%d]\n",buf[i]);
}


int main(int argc, char **argv)
{
	size_t size;
	int *array;

	srandom(time(0));

	comparisons = 0;

	// Bail if there is no argument
	if (argc != 2) {
		fprintf(stderr,"Error: supply number of elements\n");
		return 1;
	}
	size = atoi(argv[1]);	
	array = malloc(size*sizeof(int));


	// Bails if the random_array function errors
	if (random_array(array,size)) return 2;

	printf("Before sorting:\n");
	print_array(array,size);
	quicksort(array,0,size-1);

	printf("\nAfter sorting:\n");	
	print_array(array,size);

	printf("Comparisons: %d\n",comparisons);

	free(array);
	return 0;
}
