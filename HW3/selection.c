#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "quicksort.h"

static void print_array(int *buf, int size)
{
	if (size == 0) {
		fprintf(stdout,"[]\n");
		return;
	}

	int i;
	fprintf(stdout,"[");
	for (i = 0; i < size-1; i++)
		fprintf(stdout,"%d, ",buf[i]);

	fprintf(stdout,"%d]\n",buf[i]);
}


// Returns the INDEX of the pivot to use
int mdofmd(int *buf, int index, int size)
{ 
	int i;
	int med;
	int *tmpbuf;
	int *medians;

	// Something is really wrong here
	if (size < 0) {
		exit(5);
	}

	// Under 5 elements, pick a random index
	if (size < 5) {
		return random() % size;
	}

	// Adjust to number divisible by 5
//	printf("Adjusting size %d->",size);
	size -= (size % 5);	
//	printf("%d\n",size);

	tmpbuf = malloc(size * sizeof(int));
	medians = malloc((size/5) * sizeof(int));

	memcpy(tmpbuf,buf,size*sizeof(int));

	// Sort each subsection
//	printf("Sorted subsections:\n");
	for (i = 0; i < size; i += 5) {
		quicksort(buf + i, 0, 4);
//		print_array(buf+i,5);
	}


//	printf("\nMedians:\n");
	// Collect all the medians
	for (i = 2; i < size; i += 5)
		medians[(i-2)/5] = buf[i];

//	print_array(medians,size/5);
		
	free(tmpbuf);

	// Find the median of the medians
//	printf("Quick selecting the %dth element of the size %d list\n",(size/5)/2,size/5);
	med = quick_select(medians,size/5,(size/5)/2);

	free(medians);
	
	// Find the index of this median
	for (i = 0; i < size; i++) {
		if (buf[i] == med) {
			med = i;
			goto done;
		}
	}

	fprintf(stderr,"\e[05;31Something may have gone wrong in median calculation!\e[0m\n");

done:

	return med;

	//return random() % size;
}


// Note: start and end are ignored
//  Decrement index for each start increase
int quick_select(int *buf, int size, int index)
{
	int i;
	int pivot;
	int ret;
	int *recbuf;
	partition_t parts[2] = {0};	
	
//	printf("\nReceived: ");
//	print_array(buf,size);
	if (size == 0)
	{
		fprintf(stderr,"\e[05;31mSomething went wrong!\e[0m\n");
		exit(4);
	}


	pivot = mdofmd(buf,index,size);
	
	parts[0].array = calloc(1,sizeof(int) * size);
	parts[1].array = calloc(1,sizeof(int) * size);
	
	for (i = 0; i < size; i++) {
		if (i == pivot) continue;
		append_part(parts + qcomp(buf[i], buf[pivot]), buf[i]);
	}

//	printf("Index: %d\n",index);
//	printf("Left: ");	
//	print_array(parts[0].array,parts[0].size);
//	printf("Pivot: buf[%d] = %d\n",pivot,buf[pivot]);
//	printf("Right: ");	
//	print_array(parts[1].array,parts[1].size);
	
	if (parts[0].size == index) {
		//printf("\e[32mPivot is the value!\e[0m\n");
		ret = buf[pivot];
		goto cleanup;
	}
	else if (index <= (parts[0].size - 1)) {
//		printf("Recursing left %d<=%d\n",index,(parts[0].size - 1));
		recbuf = parts[0].array;
		size = parts[0].size;
	}
	else if (index >= (parts[0].size + 1)) {
//		printf("Recursing right %d>=%d\n",index,(parts[0].size + 1));
		recbuf = parts[1].array;
		index -= parts[0].size + 1;
		size = parts[1].size;
	}
	else {
//		fprintf(stderr,"\e[05;31mSomething went wrong!\e[0m\n");
		exit(2);
	} 

	ret = quick_select(recbuf,size,index);
	
cleanup:	
	free(parts[0].array);
	free(parts[1].array);

	return ret;

}

int selection(int *buf, int size, int index)
{
	int *tmpbuf;
	int ret;

	// Copy the array, to prevent destroying the original
	tmpbuf = malloc(size * sizeof(int));	
	memcpy(tmpbuf,buf, size * sizeof(int));


	ret = quick_select(tmpbuf, size, index);

	free(tmpbuf);
	return ret;
}
