int comparisons;

typedef struct {
	int* array;
	int size;
} partition_t;


int qcomp(int val, int mid);
int append_part(partition_t *pt, int value);
void quicksort(int *buf, int start, int end);
