#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "quicksort.h"

#define DEBUG 0

static void print_array(int *buf, int size)
{
	int i;
	fprintf(stderr,"[");
	for (i = 0; i < size-1; i++)
		fprintf(stderr,"%d, ",buf[i]);

	fprintf(stderr,"%d]\n",buf[i]);
}

int append_part(partition_t *pt, int value)
{
	if (NULL == pt) return -1;
	pt->array[pt->size] = value;
	(pt->size)++;

	return 0;
}


// Returns the index for either the lower or higher temp array
int qcomp(int val, int mid)
{
	comparisons++;
	return (val <= mid) ? 0 : 1;
}



void quicksort(int *buf, int start, int end)
{
	int i;
	int mid_ind;
	int mid_val;

#if DEBUG
	static int level;
	level++;
#endif
	// Base case	
	if (start >= end) {
#if DEBUG
		for (i = 0; i < level; i++) fprintf(stderr,"-");
		fprintf(stderr,"Base case, %d>=%d\n",start,end);
		--level;
#endif
		return;
	}
#if DEBUG
	for (i = 0; i < level; i++) fprintf(stderr,"-");
	fprintf(stderr,"Received: ");
	print_array(buf+start,end-start+1);
#endif
	partition_t parts[2] = {0};

	parts[0].array = calloc(1,sizeof(int)*(end-start+1));
	parts[1].array = calloc(1,sizeof(int)*(end-start+1));

	mid_ind = (random() % (end-start+1)) + start; // mid's index
	mid_val = buf[mid_ind];	


	for (i = start; i <= end; i++) {
		if (i == mid_ind) continue; // Skip the middle

		append_part(&parts[qcomp(buf[i],mid_val)],buf[i]);
	}
#if DEBUG
	//printf("sizes: %d %d\n",parts[0].size,parts[1].size);
	for (i = 0; i < level; i++) fprintf(stderr,"-");
	fprintf(stderr,"Left half: ");
	print_array(parts[0].array,parts[0].size);

	for (i = 0; i < level; i++) fprintf(stderr,"-");
	fprintf(stderr,"Mid: %d\n",mid_val);

	for (i = 0; i < level; i++) fprintf(stderr,"-");
	fprintf(stderr,"Right half: ");
	print_array(parts[1].array,parts[1].size);
#endif
	memcpy(buf+start, parts[0].array, parts[0].size * sizeof(int));
	memcpy(buf+start+parts[0].size, &mid_val, sizeof(int));
	memcpy(buf+start+parts[0].size+1, parts[1].array, parts[1].size * sizeof(int));

	// New mid_ind
	mid_ind = parts[0].size + start;


	free(parts[0].array);
	free(parts[1].array);

#if DEBUG	
	for (i = 0; i < level; i++) fprintf(stderr,"-");
	fprintf(stderr,"Before recursion: ");
	print_array(buf,end-start+1);

	for (i = 0; i < level; i++) fprintf(stderr,"-");
	fprintf(stderr,"Recursing on %d->%d\n",start,mid_ind-1);
	for (i = 0; i < level; i++) fprintf(stderr,"-");
	fprintf(stderr,"Recursing on %d->%d\n",mid_ind+1,end);
#endif
	quicksort(buf,start,mid_ind-1);
	quicksort(buf,mid_ind+1,end);

#if DEBUG
	--level;
#endif

}
