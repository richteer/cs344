#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "quicksort.h"
#include "selection.h"

static void random_array(int *buf, size_t size)
{
	int i;

	for (i = 0; i < size; i++) 
		buf[i] = random() % 100 + 1;
}

static void print_array(int *buf, int size)
{
	int i;

	printf("[");
	for (i = 0; i < size-1; i++) 
		printf("%d, ",buf[i]);

	printf("%d]\n",buf[i]);
}

static void get_suffix(char *ret, int n)
{
	switch(n%10) {
		case 1:  strncpy(ret,"st",2); return;
		case 2:  strncpy(ret,"nd",2); return;
		case 3:  strncpy(ret,"rd",2); return;
		default: strncpy(ret,"th",2); return;
	}

}


int main(int argc, char **argv)
{
	int *array, *sel_array, *qsort_array; // Random array, and its copies
	size_t size;
	int k;
	char suf[3] = {0}; // Buffer for the appropriate suffix on the number (st,nd,rd,th)
	int sel_val, q_val; // kth-values for each method
	int q_comp;

	comparisons = 0;

	if  (argc < 2) {
		fprintf(stderr,"Error: needs size of array\n");
		return 1;
	}
	srandom(clock());

	size = atoi(argv[1]);

	// Note: adjusts the array to a multiple of five
	array = malloc(size*sizeof(int) + (5 - (size % 5)));
	sel_array = malloc(size*sizeof(int) + (5 - (size % 5)));
	qsort_array = malloc(size*sizeof(int) + (5 - (size % 5)));

	if (3 == argc)
		k = atoi(argv[2]) - 1;
	else 
		k = random() % size;

	random_array(array,size);

	// Make copies of the array, as selection and quicksort are destructive
	memcpy(sel_array,array,sizeof(int)*size);
	memcpy(qsort_array,array,sizeof(int)*size);


	get_suffix(suf,k+1);
	printf("Finding the %d%s element of: \n",k+1,suf);
	print_array(array,size);
	
	quicksort(qsort_array,0,size-1);
	q_val = qsort_array[k];
	q_comp = comparisons;	

	comparisons = 0;

	printf("Sorted array is: \n");
	print_array(qsort_array,size);

	sel_val = selection(array,size,k);
	printf("Selection: the %d%s element is: %d \n",k+1,suf,sel_val);

//	print_array(qsort_array,size);
	printf("Qsort: the %d%s element is %d\n",k+1,suf,q_val);

	if (q_val == sel_val)
		printf("\e[32mSuccess!\e[0m\n");
	else
		printf("\e[31mFailed!\e[0m\n");

	free(array);
	free(sel_array);
	free(qsort_array);

	printf("QS Comparions: %d\n",q_comp);
	printf("MoM Comparisons: %d\n",comparisons);

	return 0;
}
