import sys
from subprocess import check_output as px
import matplotlib.pyplot as plt

def mean(ls):
	res = 0.0

	for l in ls:
		res += l

	return res / len(ls)

q_data = []
s_data = []

if len(sys.argv) == 2:
	mx = int(sys.argv[1])+1
else:
	mx = 1025

for i in range(2,mx):
	qline = []
	selline = []
	print("\rRunning for N={0}".format(i),end='')
	for j in range(30):
		out = px(["./selector",str(i)],universal_newlines=True).split("\n")
		qs = out[-3]
		sel = out[-2]
		qline.append(int(qs[qs.find(':')+2:]))
		selline.append(int(sel[sel.find(':')+2:]))

	q_data.append(mean(qline))
	s_data.append(mean(selline))
print("")
x = [i for i in range(2,mx)]

fig = plt.figure()
axes = fig.add_axes([0.1,0.1,0.8,0.8])
axes.plot(x,s_data,'b',label='Median of Medians')
axes.plot(x,q_data,'r',label='QuickSort')
handles, labels = axes.get_legend_handles_labels()
axes.legend(handles,labels,loc=2);

axes.set_xlabel('Size of Array')
axes.set_ylabel('# of Comparisons')
axes.set_title('Selection')
axes.legend()
fig.savefig('figure.png')


