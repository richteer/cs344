from subprocess import check_output as px
import sys
import matplotlib.pyplot as plt

cmd = ["java","MinMax"]
ran = '2..2'


def mean(ls):
	res = 0.0

	for l in ls:
		res += l

	return res / len(ls)


if len(sys.argv) != 1:
	ran = sys.argv[1]

x = [i for i in range(int(ran.split("..")[0]),int(ran.split("..")[1])+1)]

def dothething(size):
	out = px(cmd+[str(i)],universal_newlines=True).split("\n\n")

	for o in out:
		for t in o.split("\n"):
			if t.find("Recursive Comparisons") != -1:
				r = t[t.find(": ")+2:]
			elif t.find("Non-recursive Comparisons") != -1:
				nr = t[t.find(": ")+2:]

	return (int(r),int(nr))


repeats = 50

yr = []
ynr = []

for i in x:
	r = []
	nr = []
	for d in range(repeats):
		temp = dothething(i)
		r.append(temp[0])
		nr.append(temp[1])
	print("For size N = {0} array:".format(i))
	print(" Average Recursive: {0:.3f}".format(mean(r)))
	print(" Average Non-Recursive: {0:.3f}".format(mean(nr)))
	yr.append(mean(r))
	ynr.append(mean(nr))




fig = plt.figure()
axes = fig.add_axes([0.1,0.1,0.8,0.8])
axes.plot(x, yr, 'r',label="Recursive")
axes.set_xlabel('Size of Array')
axes.set_ylabel('# of Comparisons')
axes.set_title('MinMax')
#fig.savefig("recursive.png")

#fig = plt.figure()
#axes = fig.add_axes([0.1,0.1,0.8,0.8])
axes.plot(x, ynr, 'b',label="Non-Recursive")
axes.plot(x, [(3*i)/2 -2 for i in x],'g',label="3/2N - 2")

axes.legend()
#axes.set_xlabel('Size of Array')
#axes.set_ylabel('# of Comparisons')
#axes.set_title('Non-Recursive Algorithm')
fig.savefig("both")
