import java.util.ArrayList;
import java.util.Random;
import java.util.List;
import java.util.Scanner;


// Helper class that stores a (local) min and max
class pair {
	public int min;
	public int max;
	static public int N = 0;

	// Compares and store the two
	public pair(int x, int y) {
		if (x <= y) {
			min = x;
			max = y;
		}
		else {
			min = y;
			max = x;
		}
		N++;
	}

	// When a comparison is done externally.
	public pair(int m, int x, int n) {
		min = m;
		max = x;
		N += n;
	}

	// fo printin'
	public String toString() {
		return "Min: " + min + 
			   " Max: " + max;
	}

}


public class MinMax {
	public static int DEBUG = 0;
	public static int DEFAULT = 8;

	public static void main(String[] args) {
		int start = DEFAULT;
		int end = DEFAULT;

		if (args.length != 0) {
			try {
				end = start = Integer.parseInt(args[0]);
			} catch(Exception e) {
				try {
					// Get the range of array sizes from stdin.
					Scanner s = new Scanner(args[0]).useDelimiter("\\.\\.");
					start = s.nextInt();
					end = s.nextInt();
				} catch(Exception d) {
					start = DEFAULT;
					end = DEFAULT;
				}
			}

			try {
				if (args[1].equals("-v")) {
					DEBUG = 1;
				}
			} catch(Exception e) {
			}
		}
		System.out.println("Bounds: " + start + " " + end);
	
		for (int i = start; i <= end; i++) {
			dothething(i);
			
		}

	}

	// Generate a random array of size num, and calculate the Min/Max in two ways
	public static void dothething(int num){
		List<Integer> array = new ArrayList<Integer>();
		Random r = new Random();
		int rec_comp = 0;
		int non_comp = 0;

		for (int i = 0; i < num; i++) {
			array.add(Math.abs(r.nextInt() % 100));
		}
		System.out.println(array);

		int size = array.size();
		
		pair p = minMax(array,0,size-1);
		rec_comp = p.N;
		p.N = 0;
		System.out.println("rec: " + p);
		
		// Put the Non-recursive solution here.
		p = minMaxNonRec(array);
		non_comp = p.N;
		System.out.println("nonrec: " + p);
		
		System.out.println("Recursive Comparisons: " + rec_comp);
		System.out.println("Non-recursive Comparisons: " + non_comp);
		System.out.println("");
	}

	// Recursive Solution
	public static pair minMax(List<Integer> array, int start, int end) {
		if (DEBUG == 1) {
			System.out.print(array.subList(start,end+1));
		}
		
		if ((end-start) < 2) {
			pair p = new pair(array.get(start),array.get(end));
			if (DEBUG == 1) {
				System.out.println(" -> " + p);
			}
			return p;
		}

		int newstart = (end-start)/2 + start + 1;
		int newend = (end-start)/2 + start;
		
		if (DEBUG == 1) {
			System.out.println();
		}
		//System.out.println("Recursing to (" + start + " " + newend + ") and (" + newstart + " " + end + ")");
		return pairCompare(minMax(array,start,newend),minMax(array,newstart,end));	

	}

	// Non-Recursive Solution
	public static pair minMaxNonRec(List<Integer> array) {
		int max = array.get(0);
		int min = max;
		int comp = 0;
		int temp;

		for (int i = 1; i < array.size(); i++) {
			temp = array.get(i);
		
			comp++;
			if (array.get(i) >= max) {
				max = temp;
				continue; // We're done here, can't possible be min
			}
			
			comp++;
			if (array.get(i) <= min) {
				min = temp;
				continue;
			}

		}
		pair p = new pair(min,max);
		p.N = comp;

		return p;
	}

	// Compare two pairs, returns the min of the mins, max of the max
	public static pair pairCompare(pair x, pair y) {
		int tempmin;
		int tempmax;
		tempmin = (x.min <= y.min) ? x.min : y.min;
		tempmax = (x.max >= y.max) ? x.max : y.max;

		return new pair(tempmin,tempmax,2);
	}

}
